# How install

- Download and Install Node JS

- Run the following command for web:
    - npm install
    - npm run serve

- Then open your browser and enter the following address:
    - http://localhost:8080/

- Run the following command in the same foler for server socket.io:
    - node serve.js

