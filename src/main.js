import Vue from 'vue'
import App from './App.vue'
import VueSocketIO from 'vue-socket.io'
import SocketIO from 'socket.io-client'
import 'bootstrap/dist/css/bootstrap.min.css'
import '@/assets/css/main.css'
import moment from 'moment'

/* Establish Connection */
const socketConnection = SocketIO('http://localhost:3000');

Vue.use(new VueSocketIO({
  debug: true,
  connection: socketConnection
})
);


Vue.filter('formatDate', function (value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY hh:mm')
  }
});

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
