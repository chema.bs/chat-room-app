// Archivo server.js
const cors = require('cors')
const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http, {
    cors: {
        origin: '*',
    }
});

// Configuración de la aplicación Express para servir archivos estáticos (archivos HTML, CSS, etc.) desde la carpeta public/

// cors({ origin: 'http://example.com' })
app.use(express.static('public'));

// Cuando alguien se conecta al servidor mediante socket.io... 
io.on('connection', function (socket) {
    socket.on('join', user => {
        socket.user = { ...user, id: socket.client.id };
        const activeUsers = socket.client.conn.server.clientsCount;
        socket.broadcast.emit('userconnected', { activeUsers, user: socket.user });
    });

    // Se emiten los mensajes a todos los usuarios conectados en el chat room cuando alguien se une al chat room 
    // socket.broadcast.emit('user_connected');

    // Cuando un usuario envia un mensaje... 
    socket.on('message', function (data) {
        // Se emite el mensaje a todos los usuarios conectados en el chat room  
        socket.broadcast.emit('message', data);
    });

    // Cuando un usuario se desconecta del servidor...  
    socket.on('disconnectUser', () => {
        console.log('User disconnected!');
        socket.emit('left', {
            activeUsers: socket.client.conn.server.clientsCount,
            user: socket.user
        });
    });

});

// El servidor escucha peticiones en el puerto 3000  
http.listen(3000, function () { console.log('listening on *:3000'); });